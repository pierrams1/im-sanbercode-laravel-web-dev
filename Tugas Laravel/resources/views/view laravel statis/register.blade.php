<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="frst-name">First name:</label><br>
        <input type="text" name="frst-name"><br><br>
        <label for="lst-name">Last name:</label><br>
        <input type="text" name="lst-name"><br><br>
        <label for="gender">Gender:</label><br>
        <input type="radio" value="male">Male<br>
        <input type="radio" value="female">Female<br>
        <input type="radio" value="other">Other<br><br>
        <label for="negara">Nationality:</label><br>
        <select name="negara" id="">
            <option value="indonesia">Indonesian</option>
        </select><br><br>
        <label for="bahasa">Language Spoken:</label><br>
        <input type="radio" value="indonesia">Bahasa indonesia<br>
        <input type="radio" value="english">English<br>
        <input type="radio" value="other">Other<br><br>
        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>
@extends('layout.master')

@section('content')
<div class="card-body">
    <div class="card">
      <div class="card-header">
        <h1 class="card-title">Tabel</h1><br>
        <a href="/cast/create"><button type="submit" class="btn btn-primary mt-3">Create new cast</button></a>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>no</th>
              <th>name</th>
              <th>age</th>
              <th>bio</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($casts as $item => $cast)
            <tr>
              <td>{{$item + 1}}</td>
              <td>{{$cast->nama}}</td>
              <td>{{$cast->umur}}</td>
              <td>{{$cast->bio}}</td>
              <td class="d-flex">
                  <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm mr-2">detail</a>
                  <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm mr-2">edit</a>
                  <form action="/cast/{{$cast->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <input type="submit" value="delete" class="btn btn-danger btn-sm">
                  </form>
              </td>
            </tr> 
            @empty
            <tr>
              <td colspan="4" align="center">No Cast</td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
@endsection
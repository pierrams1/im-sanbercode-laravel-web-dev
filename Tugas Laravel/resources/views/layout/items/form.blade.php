@extends('layout.master')

@section('content')
<div class="container pt-3 col-8">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create New Cast</h3>
        </div>
        <form role="form" action="/cast" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInput">Name</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '') }}" placeholder="Enter name">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror

                <div class="form-group">
                    <label for="exampleInput">umur</label>
                    <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur'. '') }}"placeholder="age">
                </div>
                @error('umur')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror

                <div class="form-group">
                    <label for="exampleInput">bio</label>
                    <textarea type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}"placeholder="bio"></textarea>
                </div>
                @error('bio')
                    <div class="alert alert-danger">{{ $message}}</div>
                @enderror
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" fdprocessedid="c9uhud">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection
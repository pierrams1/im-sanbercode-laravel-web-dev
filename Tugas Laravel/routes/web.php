<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.master');
});

Route::get('/table', function () {
    return view('layout.items.table');
});

Route::get('/form', function () {
    return view('layout.items.form');
});

// Route::get('/', [AuthController::class, 'index']);

Route::get('/Register', [AuthController::class, 'register']);

Route::post('/welcome', [HomeController::class, 'welcome']);

// 

Route::get('/cast', [CastController::class, 'index']);

Route::get('/cast/create', [CastController::class, 'create']);

Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast/{cast_id}',  [CastController::class, 'detail']);

Route::get('/cast/{cast_id}/edit',  [CastController::class, 'edit']);

Route::put('/cast/{cast_id}', [CastController::class, 'update']);

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);


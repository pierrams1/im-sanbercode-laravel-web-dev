<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    function welcome(Request $request){
        // dd($request->all());
        $first_name = $request["frst-name"];
        $last_name = $request["lst-name"];
        // dd($first_name);
        return view('respon', ['first_name'=>$first_name, "last_name"=>$last_name]);
    }
}

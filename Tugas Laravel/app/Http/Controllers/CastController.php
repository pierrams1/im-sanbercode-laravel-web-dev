<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    //
    public function index(){
        $casts = DB::table('casts')->get();
        // dd($cast)->all();
        return view('layout.items.table', compact('casts'));
    }

    public function create(request $request){
        return view('layout.items.form');
    }

    public function store(request $request){
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')->insert([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Cast berhasil disimpan!');
    }

    public function detail($request){
        // dd($request);
        $cast = DB::table('casts')->where('id', $request)->first();
        return view('layout.items.detail', compact('cast'));
    }

    public function edit($request){
        // dd($request);
        $cast = DB::table('casts')->where('id', $request)->first();
        return view('layout.items.edit', compact('cast'));

    }

    public function update(request $request, $cast_id){
        $request = $request->all();
        // dd($request, $cast_id);
        $query = DB::table('casts')->where('id', $cast_id)->update([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Berhasil update data!');
    }

    public function destroy($cast_id){
        // dd($cast_id);
        $query = DB::table('casts')->where('id',$cast_id)->delete();
        return redirect('cast')->with('success', 'Berhasil delete data!');
    }
}

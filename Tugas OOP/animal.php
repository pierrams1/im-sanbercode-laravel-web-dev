<?php
class animal{
    private $name;
    private $legs = 4;
    private $cold_blood = 'no';
    public function __construct($string) 
    {
        $this->name = $string;
    }

    public function getname(){
        return $this->name;
    }

    public function setname($string){
        $this->name = $string;
    }

    public function getlegs(){
        return $this->legs;
    }

    public function setlegs($string){
        $this->legs = $string;
    }

    public function getcold_blood(){
        return $this->cold_blood;
    }

    public function setcold_blood($string){
        $this->name = $string;
    }
}
?>
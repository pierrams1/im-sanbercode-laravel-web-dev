<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$shaun = new animal('shaun');
$buduk = new Frog('buduk');
$kera_sakti = new Ape('kera sakti');

echo "Name: " . $shaun->getname() . "<br>";
echo "Legs: " . $shaun->getlegs() . "<br>";
echo "Cold Blood: " . $shaun->getcold_blood() . "<br><br>";

echo "Name: " . $buduk->getname() . "<br>";
echo "Legs: " . $buduk->getlegs() . "<br>";
echo "Cold Blood: " . $buduk->getcold_blood() . "<br>";
echo "Jump: " . $buduk->jump() . "<br><br>";

echo "Name: " . $kera_sakti->getname() . "<br>";
echo "Legs: " . $kera_sakti->getlegs() . "<br>";
echo "Cold Blood: " . $kera_sakti->getcold_blood() . "<br>";
echo "Yell: " . $kera_sakti->yell() . "<br><br>";
?>